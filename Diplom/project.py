import numpy as np
import scapy.all as scapy
from scapy.layers import http
import multiprocessing
from tkinter import *
import time
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox as mb
import os
import datetime

def start():
    for i in range(len(our_mac)):
        multiprocessing.Process(target=sniff, name = f'Process_{our_mac[i]}', args=('wlo1', our_mac[i])).start()
        print(f"process {i} is started for mac {our_mac[i]}")
        if i == 0:
            break

def clearTabs():
    list = All_db_view.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 2:
            l.destroy()

def showDb():
    clearTabs()
    with open("db.txt", 'r') as read_file:
        data = read_file.readlines()
    Data_List = list()
    for i in range(len(data)):
        Data_List.append(data[i].split())

    Lbl1 = Label(All_db_view, text = 'ФИО')
    Lbl2 = Label(All_db_view, text = 'МакВМ')
    Lbl3 = Label(All_db_view, text = 'Статус')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Label_list = list()
    for i in range(len(Data_List)):
        for j in range(len(Data_List[i])):
            if Data_List[i][j] == 'Удовлетворительно':
                Label_list.append(Label(All_db_view, text = Data_List[i][j]))
            elif Data_List[i][j] == 'Вопрос':
                Label_list.append(Label(All_db_view, text = Data_List[i][j], fg = 'yellow'))
            elif Data_List[i][j] == 'Нарушение':
                Label_list.append(Label(All_db_view, text = Data_List[i][j], fg = 'red'))
            else:
                Label_list.append(Label(All_db_view, text = Data_List[i][j]))
    j = 2
    for i in range(0,len(Label_list), 3):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        j += 1
    All_db_view.after(5000, showDb)

def reaction(react, name):
    match react:
        case 'У':
            print(name, 'Удовлетворительно')
            pass
        case 'П':
            print(name, 'Вопрос')
            now = datetime.datetime.now()
            d = open('log.txt', 'a')
            d.writelines(f'{name}, Вопрос {now}\n')
            d.close()
            f = open('db.txt', 'r')
            data = f.readlines()
            f.close()
            for i in range(len(data)):
                if name in data[i]:
                    if 'Удовлетворительно' in data[i]:
                        data[i] = data[i].replace('Удовлетворительно', 'Вопрос')
            f = open('db.txt', 'w')
            for i in range(len(data)):
                f.write(data[i])
            f.close()
        case 'Н':
            print(name, 'Нарушение')
            now = datetime.datetime.now()
            d = open('log.txt', 'a')
            d.writelines(f'{name}, Нарушение {now}\n')
            d.close()
            f = open('db.txt', 'r')
            data = f.readlines()
            f.close()
            for i in range(len(data)):
                if name in data[i]:
                    if 'Удовлетворительно' in data[i]:
                        data[i] = data[i].replace('Удовлетворительно', 'Нарушение')
                    if 'Вопрос' in data[i]:
                        data[i] = data[i].replace('Вопрос', 'Нарушение')
            f = open('db.txt', 'w')
            for i in range(len(data)):
                f.write(data[i])
            f.close()
            a = os.system(f'sudo su andrey /home/andrey/Desktop/Sniffer/checkVm.sh {name}')
            if a == 1:
                try:
                    os.system(f'sudo su andrey /home/andrey/Desktop/Sniffer/killVm.sh {name}')
                except:                        pass
            multiprocessing.Process.kill(f'Process_{name}')
    print('--------')

def sigmoid(x):
    # Наша функция активации: f(x) = 1 / (1 + e^(-x))
    return 1 / (1 + np.exp(-x))

def deriv_sigmoid(x):
    # Производная от sigmoid: f'(x) = f(x) * (1 - f(x))
    fx = sigmoid(x)
    return fx * (1 - fx)
 
def mse_loss(y_true, y_pred):
    # y_true и y_pred являются массивами numpy с одинаковой длиной
    return ((y_true - y_pred) ** 2).mean()
 
class OurNeuralNetwork:
    """
    Нейронная сеть, у которой: 
        - 2 входа
        - 1 скрытый слой с двумя нейронами (h1, h2) 
        - слой вывода с одним нейроном (o1)
    У каждого нейрона одинаковые вес и смещение:
        - w = [0, 1]
        - b = 0
    """
    def __init__(self):
        f = open("weights.txt",'r')
        weights = f.readlines()
        f.close()
        weights_float = list()
        for i in range(len(weights)):
            weights_float.append(float(weights[i]))
        '''
        self.w1 = np.random.normal()
        self.w2 = np.random.normal()
        self.w3 = np.random.normal()
        self.w4 = np.random.normal()
        self.w5 = np.random.normal()
        self.w6 = np.random.normal()
        self.w7 = np.random.normal()
        self.w8 = np.random.normal()
        self.w9 = np.random.normal()
        self.w10 = np.random.normal()

        self.b1 = np.random.normal()
        self.b2 = np.random.normal()
        self.b3 = np.random.normal()
        '''
        self.w1 = weights_float[0]
        self.w2 = weights_float[1]
        self.w3 = weights_float[2]
        self.w4 = weights_float[3]
        self.w5 = weights_float[4]
        self.w6 = weights_float[5]
        self.w7 = weights_float[6]
        self.w8 = weights_float[7]
        self.w9 = weights_float[8]
        self.w10 = weights_float[9]

        self.b1 = weights_float[10]
        self.b2 = weights_float[11]
        self.b3 = weights_float[12]
 
    def feedforward(self, x):     
        h1 = sigmoid(self.w1 * x[0] + self.w2 * x[1] + self.w3 * x[2] + self.w4 * x[3] + self.b1)
        h2 = sigmoid(self.w5 * x[0] + self.w6 * x[1] + self.w7 * x[2] + self.w8 * x[3] + self.b2)
        o1 = sigmoid(self.w9 * h1 + self.w10 * h2 + self.b3)
        if 1 - o1 < 0.1:
            answer = 'У'
        elif 1 - o1 < 0.6 and 1 - o1 > 0.4:
            answer = 'П'
        else:
            answer = 'Н'
        return answer 
        
    def train(self, data, all_y_trues):
        """
        - data is a (n x 2) numpy array, n = # of samples in the dataset.
        - all_y_trues is a numpy array with n elements.
            Elements in all_y_trues correspond to those in data.
        """
        learn_rate = 0.001
        epochs = 1000000 # количество циклов во всём наборе данных
 
        for epoch in range(epochs):
            for x, y_true in zip(data, all_y_trues):
                # --- Выполняем обратную связь (нам понадобятся эти значения в дальнейшем)
                sum_h1 = self.w1 * x[0] + self.w2 * x[1] + self.w3 * x[2] + self.w4 * x[3] + self.b1
                h1 = sigmoid(sum_h1)
 
                sum_h2 = self.w5 * x[0] + self.w6 * x[1] + self.w7 * x[2] + self.w8 * x[3] + self.b2
                h2 = sigmoid(sum_h2)
 
                sum_o1 = self.w9 * h1 + self.w10 * h2 + self.b3
                o1 = sigmoid(sum_o1)
                y_pred = o1
 
                # --- Подсчет частных производных
                # --- Наименование: d_L_d_w1 представляет "частично L / частично w1"
                d_L_d_ypred = -2 * (y_true - y_pred)
 
                # Нейрон o1
                d_ypred_d_w9 = h1 * deriv_sigmoid(sum_o1)
                d_ypred_d_w10 = h2 * deriv_sigmoid(sum_o1)
                d_ypred_d_b3 = deriv_sigmoid(sum_o1)

                d_ypred_d_h1 = self.w9 * deriv_sigmoid(sum_o1)
                d_ypred_d_h2 = self.w10 * deriv_sigmoid(sum_o1)
 
                # Нейрон h1
                d_h1_d_w1 = x[0] * deriv_sigmoid(sum_h1)
                d_h1_d_w2 = x[1] * deriv_sigmoid(sum_h1)
                d_h1_d_w3 = x[2] * deriv_sigmoid(sum_h1)
                d_h1_d_w4 = x[3] * deriv_sigmoid(sum_h1)
                d_h1_d_b1 = deriv_sigmoid(sum_h1)
 
                # Нейрон h2
                d_h2_d_w5 = x[0] * deriv_sigmoid(sum_h2)
                d_h2_d_w6 = x[1] * deriv_sigmoid(sum_h2)
                d_h2_d_w7 = x[2] * deriv_sigmoid(sum_h2)
                d_h2_d_w8 = x[3] * deriv_sigmoid(sum_h2)
                d_h2_d_b2 = deriv_sigmoid(sum_h2)

                # --- Обновляем вес и смещения
                # Нейрон h1
                self.w1 -= learn_rate * d_L_d_ypred * d_ypred_d_h1 * d_h1_d_w1
                self.w2 -= learn_rate * d_L_d_ypred * d_ypred_d_h1 * d_h1_d_w2
                self.w3 -= learn_rate * d_L_d_ypred * d_ypred_d_h1 * d_h1_d_w3
                self.w4 -= learn_rate * d_L_d_ypred * d_ypred_d_h1 * d_h1_d_w4
                self.b1 -= learn_rate * d_L_d_ypred * d_ypred_d_h1 * d_h1_d_b1
 
                # Нейрон h2
                self.w5 -= learn_rate * d_L_d_ypred * d_ypred_d_h2 * d_h2_d_w5
                self.w6 -= learn_rate * d_L_d_ypred * d_ypred_d_h2 * d_h2_d_w6
                self.w7 -= learn_rate * d_L_d_ypred * d_ypred_d_h2 * d_h2_d_w7
                self.w8 -= learn_rate * d_L_d_ypred * d_ypred_d_h2 * d_h2_d_w8
                self.b2 -= learn_rate * d_L_d_ypred * d_ypred_d_h2 * d_h2_d_b2
 
                # Нейрон o1
                self.w9 -= learn_rate * d_L_d_ypred * d_ypred_d_w9
                self.w10 -= learn_rate * d_L_d_ypred * d_ypred_d_w10
                self.b3 -= learn_rate * d_L_d_ypred * d_ypred_d_b3
 
            # --- Подсчитываем общую потерю в конце каждой фазы
            if epoch % 10 == 0:
                y_preds = np.apply_along_axis(self.feedforward, 1, data)
                loss = mse_loss(all_y_trues, y_preds)
                print("Epoch %d loss: %.3f" % (epoch, loss))
        print("Вес первого узла: ", self.w1, self.w2, self.w3, self.w4)
        print("Вес второго узла: ", self.w5, self.w6, self.w7, self.w8)
        print("Вес конечного узла: ", self.w9, self.w10)
        print("Смещения: ", self.b1, self.b2, self.b3)
        f = open('weights.txt', 'w')
        f.write(str(self.w1) + '\n')
        f.write(str(self.w2) + '\n')
        f.write(str(self.w3) + '\n')
        f.write(str(self.w4) + '\n')
        f.write(str(self.w5) + '\n')
        f.write(str(self.w6) + '\n')
        f.write(str(self.w7) + '\n')
        f.write(str(self.w8) + '\n')
        f.write(str(self.w9) + '\n')
        f.write(str(self.w10) + '\n')
        f.write(str(self.b1) + '\n')
        f.write(str(self.b2) + '\n')
        f.write(str(self.b3) + '\n')
        f.close()

def sniff(inface, name):
    scapy.sniff(iface=inface, store=False, prn=process_sniffed_packet, filter=f'ether host {name}')
    time.sleep(0.1)

def process_sniffed_packet(packet):
    data_for_neironet = [0, 0, 0, 0]
    try:
        data_for_neironet[1] = 1
        if packet.dst in our_mac:
            data_for_neironet[0] = 1
            data_for_neironet[2] = 1
        else:
            if packet.getlayer('IP').dst in allow_ip:
                data_for_neironet[2] = 1
            elif packet.getlayer('IP').dst in wrong_ip:
                data_for_neironet[2] = -1
            else:
                data_for_neironet[2] = 0
        if str(packet.sport) in ports:
            data_for_neironet[3] = 0
        else:
            data_for_neironet[3] = 1
        react = network.feedforward(data_for_neironet)
        print(packet.getlayer('IP').dst, packet.src, react)
        multiprocessing.Process(target=reaction, args=(react, packet.src)).start()
    except:
        pass

f = open("our_mac.txt","r")
our_mac = f.read().splitlines()
f.close()

f1 = open("allows_ip.txt","r")
allow_ip = f1.read().splitlines()
f1.close()

f2 = open("wrong_ip.txt","r")
wrong_ip = f2.read().splitlines()
f2.close()

f3 = open("port_and_protocols.txt", 'r')
ports = f3.read().splitlines()
f3.close()

data_for_neironet = np.array([0, 0, 0, 0])

network = OurNeuralNetwork()

data_for_education = np.array([
    [1, 0, 1, 1],    
    [0, 1, 1, 1],
    [1, 0, 1, 0],     
    [0, 1, 1, 0],
    [1, 0, 0, 1],    
    [0, 1, 0, 1],
    [1, 0, 0, 0],     
    [0, 1, 0, 0],
    [1, 0, -1, 1],    
    [0, 1, -1, 1],
    [1, 0, -1, 0],     
    [0, 1, -1, 0],
    [1, 1, 1, 1],    
    [1, 1, 1, 1],
    [1, 1, 1, 0],     
    [1, 1, 1, 0]
])
 
all_y_trues = np.array([
    1,
    1,
    1,
    1,
    0.5,
    0.5,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    1,
    1,
    1
])


'''
# Тренируем нашу нейронную сеть!
print(network.feedforward(data_for_education[0]))
print(network.feedforward(data_for_education[1]))
print(network.feedforward(data_for_education[2]))
print(network.feedforward(data_for_education[3]))
print(network.feedforward(data_for_education[4]))
print(network.feedforward(data_for_education[5]))
print(network.feedforward(data_for_education[6]))
print(network.feedforward(data_for_education[7]))
print(network.feedforward(data_for_education[8]))
print(network.feedforward(data_for_education[9]))
print(network.feedforward(data_for_education[10]))
print(network.feedforward(data_for_education[11]))
print(network.feedforward(data_for_education[12]))
print(network.feedforward(data_for_education[13]))
print(network.feedforward(data_for_education[14]))
print(network.feedforward(data_for_education[15]))
print('--------------------------------')
network.train(data_for_education, all_y_trues)
print('--------------------------------')
print(network.feedforward(data_for_education[0]))
print(network.feedforward(data_for_education[1]))
print(network.feedforward(data_for_education[2]))
print(network.feedforward(data_for_education[3]))
print(network.feedforward(data_for_education[4]))
print(network.feedforward(data_for_education[5]))
print(network.feedforward(data_for_education[6]))
print(network.feedforward(data_for_education[7]))
print(network.feedforward(data_for_education[8]))
print(network.feedforward(data_for_education[9]))
print(network.feedforward(data_for_education[10]))
print(network.feedforward(data_for_education[11]))
print(network.feedforward(data_for_education[12]))
print(network.feedforward(data_for_education[13]))
print(network.feedforward(data_for_education[14]))
print(network.feedforward(data_for_education[15]))

'''

root = Tk()
root.geometry('960x540')
root.title('Программа')
tab_control = ttk.Notebook(root)
All_db_view = ttk.Frame(tab_control)
tab_control.add(All_db_view, text = 'База сотрудников')
Allarms = ttk.Frame(root)
tab_control.pack(expand=1, fill = 'both')
Button_for_start = Button(All_db_view, text = 'Старт', command = start)
Button_for_show_db = Button(All_db_view, text = 'Отображение', command = showDb)

Button_for_start.grid(row = 0, column = 0)
Button_for_show_db.grid(row = 0, column = 1)

root.mainloop()
