import psycopg2
from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox as mb


def updatewithcursor():
    def add():
        numb = Ent1.get()
        value = Ent2.get()
        try:
            cur = con.cursor()
            cur.execute(f"select cursor_help_update({numb}, {value})")
            cur.close()
            con.commit()
        except:
            con.rollback()
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()


    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Обновление раб.группы курсором')
    Lbl1 = Label(root2, text = 'Введите старый номер:')
    Lbl2 = Label(root2, text = 'Введите новый номер:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 1, column = 0)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 1, column = 1)
    Btn1.grid(row = 2, column = 0)    
def showResearches():
    clearResearches()
    Lbl1 = Label(TabResearches, text = 'Номер')
    Lbl2 = Label(TabResearches, text = 'Название')
    Lbl3 = Label(TabResearches, text = 'Локация')
    Lbl4 = Label(TabResearches, text = 'Ур.допуска')
    Lbl5 = Label(TabResearches, text = 'Ответственный')
    Lbl6 = Label(TabResearches, text = 'Список оборудования')
    Lbl7 = Label(TabResearches, text = 'Номер раб.группы')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    Lbl5.grid(row = 1, column = 4)
    Lbl6.grid(row = 1, column = 5)
    Lbl7.grid(row = 1, column = 6)
    cur = con.cursor()
    cur.execute("select * from researches order by number")
    rows = cur.fetchall()
    cur.close()
    List = list()
    Label_list = list()
    for i in range(len(rows)):
        List.append(rows[i])
    for i in range(len(List)):
        for j in range(len(List[i])):
            if j == len(List[i]) - 1:
                if List[i][j] != 1 and List[i][j] != 2 and List[i][j] != 3 and List[i][j] != 4:
                    Label_list.append(Label(TabResearches, text = 'Нет'))
                else:
                    Label_list.append(Label(TabResearches, text = List[i][j]))
            else:
                Label_list.append(Label(TabResearches, text = List[i][j]))
    j = 2
    for i in range(0, len(List) * 7, 7):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        Label_list[i + 4].grid(row = j, column = 4)
        Label_list[i + 5].grid(row = j, column = 5)
        Label_list[i + 6].grid(row = j, column = 6)
        j += 1
def clearResearches():
    list = TabResearches.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 5:
            l.destroy()
def addResearches():
    def add():
        name = Ent1.get()
        location = Ent2.get()
        allow_level = Ent3.get()
        responsible = Ent4.get()
        list = Ent5.get()
        group_number = Ent6.get()
        try:
            cur = con.cursor()
            cur.execute(f"call add_to_researches('{name}',{location},{allow_level},{responsible},{list},{group_number})")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()



    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление сотрудников')
    Lbl1 = Label(root2, text = 'Введине название:')
    Lbl2 = Label(root2, text = 'Введите локацию:')
    Lbl3 = Label(root2, text = 'Введите ур.доступа:')
    Lbl4 = Label(root2, text = 'Введите ответственного:')
    Lbl5 = Label(root2, text = 'Введите список оборудования:')
    Lbl6 = Label(root2, text = 'Введите раб.группу:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Ent3 = Entry(root2, width = 30)
    Ent4 = Entry(root2, width = 30)
    Ent5 = Entry(root2, width = 30)
    Ent6 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 1, column = 0)
    Lbl3.grid(row = 2, column = 0)
    Lbl4.grid(row = 3, column = 0)
    Lbl5.grid(row = 4, column = 0)
    Lbl6.grid(row = 5, column = 0)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 1, column = 1)
    Ent3.grid(row = 2, column = 1)
    Ent4.grid(row = 3, column = 1)
    Ent5.grid(row = 4, column = 1)
    Ent6.grid(row = 5, column = 1)
    Btn1.grid(row = 6, column = 1)
def updateResearches():
    def add():
        numb = Ent1.get()
        name = Ent2.get()
        location = Ent3.get()
        allow_level = Ent4.get()
        responsible = Ent5.get()
        list = Ent6.get()
        group_number = Ent7.get()
        try:
            cur = con.cursor()
            if len(name) > 0:
                cur.execute(f"call update_researches1({numb}, '{name}')")
            if len(location) > 0:
                cur.execute(f"call update_researches2({numb}, {location})")
            if len(allow_level) > 0:
                cur.execute(f"call update_researches3({numb}, {allow_level})")
            if len(responsible) > 0:
                cur.execute(f"call update_researches4({numb}, {responsible})")
            if len(list) > 0:
                cur.execute(f"call update_researches5({numb}, {list})")
            if len(group_number) > 0:
                cur.execute(f"call update_researches6({numb}, {group_number})")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()



    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление исследований')
    Lbl1 = Label(root2, text = 'Введине номер:')
    Lbl2 = Label(root2, text = 'Введине название:')
    Lbl3 = Label(root2, text = 'Введите локацию:')
    Lbl4 = Label(root2, text = 'Введите ур.доступа:')
    Lbl5 = Label(root2, text = 'Введите ответственного:')
    Lbl6 = Label(root2, text = 'Введите список оборудования:')
    Lbl7 = Label(root2, text = 'Введите раб.группу:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Ent3 = Entry(root2, width = 30)
    Ent4 = Entry(root2, width = 30)
    Ent5 = Entry(root2, width = 30)
    Ent6 = Entry(root2, width = 30)
    Ent7 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 0, column = 2)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 2, column = 2)
    Lbl5.grid(row = 3, column = 2)
    Lbl6.grid(row = 4, column = 2)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 0, column = 3)
    Ent3.grid(row = 1, column = 3)
    Ent4.grid(row = 2, column = 3)
    Ent5.grid(row = 3, column = 3)
    Ent6.grid(row = 4, column = 3)
    Ent7.grid(row = 5, column = 3)
    Btn1.grid(row = 6, column = 2)
    Lbl7.grid(row = 5, column = 2)
def deleteResearches():
    def delete():
        numb = Ent1.get()
        try:
            cur = con.cursor()
            cur.execute(f"call delete_from_researches({numb})")
            cur.close()
            con.commit()
        except:
            con.rollback()
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения.\n Проверьте, есть ли связи удаляемой записи с другими таблицами.")
        root2.destroy()


    root2 = Tk()
    root2.geometry('480x270')
    root2.title('Удаление исследований')
    Lbl1 = Label(root2, text = 'Введите номер:')
    Ent1 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Удалить', command = delete)
    Lbl1.grid(row = 0, column = 0)
    Ent1.grid(row = 0, column = 1)
    Btn1.grid(row = 0, column = 2)

def showMembers():
    clearMembers()
    Lbl1 = Label(TabMembers, text = 'Номер')
    Lbl2 = Label(TabMembers, text = 'Фамилия')
    Lbl3 = Label(TabMembers, text = 'Имя')
    Lbl4 = Label(TabMembers, text = 'Отчество')
    Lbl5 = Label(TabMembers, text = 'Ур.доступа')
    Lbl6 = Label(TabMembers, text = 'Раб.группа')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    Lbl5.grid(row = 1, column = 4)
    Lbl6.grid(row = 1, column = 5)
    cur = con.cursor()
    cur.execute("select * from members order by number")
    rows = cur.fetchall()
    cur.close()
    List = list()
    Label_list = list()
    for i in range(len(rows)):
        List.append(rows[i])
    for i in range(len(List)):
        for j in range(len(List[i])):
            if j == len(List[i]) - 1:
                if List[i][j] != 1 and List[i][j] != 2 and List[i][j] != 3 and List[i][j] != 4:
                    Label_list.append(Label(TabMembers, text = 'Нет'))
                else:
                    Label_list.append(Label(TabMembers, text = List[i][j]))
            else:
                Label_list.append(Label(TabMembers, text = List[i][j]))
    j = 2
    for i in range(0, len(List) * 6, 6):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        Label_list[i + 4].grid(row = j, column = 4)
        Label_list[i + 5].grid(row = j, column = 5)
        j += 1
def clearMembers():
    list = TabMembers.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 4:
            l.destroy()
def addMembers():
    def add():
        surname = Ent1.get()
        name = Ent2.get()
        patronymic = Ent3.get()
        allow_level = Ent4.get()
        group_number = Ent5.get()
        try:
            cur = con.cursor()
            cur.execute(f"call add_to_member('{surname}', '{name}', '{patronymic}', {allow_level}, {group_number})")
            cur.close()
            con.commit()
            root2.destroy()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
    
    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление сотрудника')
    Lbl1 = Label(root2, text = 'Введите Фамилию:')
    Lbl2 = Label(root2, text = 'Введите Имя:')
    Lbl3 = Label(root2, text = 'Введите Отчество:')
    Lbl4 = Label(root2, text = 'Введите ур.доступа:')
    Lbl5 = Label(root2, text = 'Введите раб.группу:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Ent3 = Entry(root2, width = 30)
    Ent4 = Entry(root2, width = 30)
    Ent5 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 1, column = 0)
    Lbl3.grid(row = 2, column = 0)
    Lbl4.grid(row = 3, column = 0)
    Lbl5.grid(row = 4, column = 0)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 1, column = 1)
    Ent3.grid(row = 2, column = 1)
    Ent4.grid(row = 3, column = 1)
    Ent5.grid(row = 4, column = 1)
    Btn1.grid(row = 5, column = 0)
def updateMembers():
    def add():
        numb = Ent1.get()
        surname = Ent2.get()
        name = Ent3.get()
        patronymic = Ent4.get()
        allow_level = Ent5.get()
        group_number = Ent6.get()
        try:
            cur = con.cursor()
            if len(surname) > 0:
                cur.execute(f"call update_members1({numb}, '{surname}')")
            if len(name) > 0:
                cur.execute(f"call update_members2({numb}, '{name}')")
            if len(patronymic) > 0:
                cur.execute(f"call update_members3({numb}, '{patronymic}')")
            if len(allow_level) > 0:
                cur.execute(f"call update_members4({numb}, {allow_level})")
            if len(group_number) > 0:
                cur.execute(f"call update_members5({numb}, {group_number})")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()



    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление сотрудников')
    Lbl1 = Label(root2, text = 'Введине номер:')
    Lbl2 = Label(root2, text = 'Введите фамилию:')
    Lbl3 = Label(root2, text = 'Введите имя:')
    Lbl4 = Label(root2, text = 'Введите отчество:')
    Lbl5 = Label(root2, text = 'Введите ур.доступа:')
    Lbl6 = Label(root2, text = 'Введите раб.группу:')
    Lbl7 = Label(root2, text = 'Обратите внимание, если номер раб.группы не равен 1,2,3,4, то тогда сотрудник не имеет раб.группы')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Ent3 = Entry(root2, width = 30)
    Ent4 = Entry(root2, width = 30)
    Ent5 = Entry(root2, width = 30)
    Ent6 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 0, column = 2)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 2, column = 2)
    Lbl5.grid(row = 3, column = 2)
    Lbl6.grid(row = 4, column = 2)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 0, column = 3)
    Ent3.grid(row = 1, column = 3)
    Ent4.grid(row = 2, column = 3)
    Ent5.grid(row = 3, column = 3)
    Ent6.grid(row = 4, column = 3)
    Btn1.grid(row = 5, column = 2)
    Lbl7.grid(row = 7, column = 0, columnspan = 4)
def deleteMembers():
    def delete():
        numb = Ent1.get()
        try:
            cur = con.cursor()
            cur.execute(f"call delete_from_members({numb})")
            cur.close()
            con.commit()
        except:
            con.rollback()
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения.\n Проверьте, есть ли связи удаляемой записи с другими таблицами.")
        root2.destroy()


    root2 = Tk()
    root2.geometry('480x270')
    root2.title('Удаление сотрудников')
    Lbl1 = Label(root2, text = 'Введите номер:')
    Ent1 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Удалить', command = delete)
    Lbl1.grid(row = 0, column = 0)
    Ent1.grid(row = 0, column = 1)
    Btn1.grid(row = 0, column = 2)

def showLocations():
    clearLocations()
    Lbl1 = Label(TabLocations, text = 'Номер')
    Lbl2 = Label(TabLocations, text = 'Название')
    Lbl3 = Label(TabLocations, text = 'Ур.доступа')
    Lbl4 = Label(TabLocations, text = 'ответственный')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select * from locations order by number")
    rows = cur.fetchall()
    cur.close()
    List = list()
    Label_list = list()
    for i in range(len(rows)):
        List.append(rows[i])
    for i in range(len(List)):
        for j in range(len(List[i])):
            Label_list.append(Label(TabLocations, text = List[i][j]))
    j = 2
    for i in range(0, len(List) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clearLocations():
    list = TabLocations.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 4:
            l.destroy()
def addLocations():
    def add():
        name = Ent1.get()
        allow_level = Ent2.get()
        responsible = Ent3.get()
        try:
            cur = con.cursor()
            cur.execute(f"call add_to_locations('{name}',{allow_level},{responsible})")
            cur.close()
            con.commit()
            root2.destroy()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
    
    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление оборудования')
    Lbl1 = Label(root2, text = 'Введите название:')
    Lbl2 = Label(root2, text = 'Введите ур.доступа:')
    Lbl3 = Label(root2, text = 'Введите ответственного:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Ent3 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 1, column = 0)
    Lbl3.grid(row = 2, column = 0)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 1, column = 1)
    Ent3.grid(row = 2, column = 1)
    Btn1.grid(row = 5, column = 0)        
def updateLocations():
    def add():
        numb = Ent1.get()
        name = Ent2.get()
        allow_level = Ent3.get()
        responsible = Ent4.get()
        try:
            cur = con.cursor()
            if len(name) > 0:
                cur.execute(f"call update_locations1({numb}, '{name}')")
            if len(allow_level) > 0:
                cur.execute(f"call update_locations2({numb}, {allow_level})")
            if len(responsible) > 0:
                cur.execute(f"call update_locations3({numb}, {responsible})")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()



    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление оборудования')
    Lbl1 = Label(root2, text = 'Введине номер:')
    Lbl2 = Label(root2, text = 'Введите название:')
    Lbl3 = Label(root2, text = 'Введите ур.доступа:')
    Lbl4 = Label(root2, text = 'Введите ответственного:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Ent3 = Entry(root2, width = 30)
    Ent4 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 0, column = 2)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 2, column = 2)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 0, column = 3)
    Ent3.grid(row = 1, column = 3)
    Ent4.grid(row = 2, column = 3)
    Btn1.grid(row = 3, column = 2) 
def deleteLocations():
    def delete():
        numb = Ent1.get()
        try:
            cur = con.cursor()
            cur.execute(f"call delete_from_locations({numb})")
            cur.close()
            con.commit()
        except:
            con.rollback()
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения.\n Проверьте, есть ли связи удаляемой записи с другими таблицами.")
        root2.destroy()


    root2 = Tk()
    root2.geometry('480x270')
    root2.title('Удаление локаций')
    Lbl1 = Label(root2, text = 'Введите номер:')
    Ent1 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Удалить', command = delete)
    Lbl1.grid(row = 0, column = 0)
    Ent1.grid(row = 0, column = 1)
    Btn1.grid(row = 0, column = 2)

def showLists():
    clearLists()
    Lbl1 = Label(TabLists, text = 'Номер')
    Lbl2 = Label(TabLists, text = 'Название')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    cur = con.cursor()
    cur.execute("select * from listofequipment order by number")
    rows = cur.fetchall()
    cur.close()
    List = list()
    Label_list = list()
    for i in range(len(rows)):
        List.append(rows[i])
    for i in range(len(List)):
        for j in range(len(List[i])):
            Label_list.append(Label(TabLists, text = List[i][j]))
    j = 2
    for i in range(0, len(List) * 2, 2):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        j += 1
def clearLists():
    list = TabLists.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 4:
            l.destroy()
def addLists():
        def add2():
            numb = Ent2.get()
            numb1 = Ent3.get()
            try:
                cur = con.cursor()
                cur.execute(f"call add_to_inlist('{numb}','{numb1}')")
                cur.close()
                con.commit()
            except:
                mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
            root2.destroy()

        def add():
            numb = Ent1.get()
            try:
                cur = con.cursor()
                cur.execute(f"call add_to_listofequipment('{numb}')")
                cur.close()
                con.commit()
            except:
                mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
            root2.destroy()


        root2 = Tk()
        root2.geometry('960x540')
        root2.title('Добавление списка оборудования|оборудования в список')
        Lbl1 = Label(root2, text = 'Введите значение:')
        Ent1 = Entry(root2, width = 30)
        Btn1 = Button(root2, text = 'Добавить', command = add)
        Lbl1.grid(row = 0, column = 0)
        Ent1.grid(row = 0, column = 1)
        Btn1.grid(row = 0, column = 2)
        
        Lbl2 = Label(root2, text = 'Введите номер списка:')
        Lbl3 = Label(root2, text = 'Введите номер оборудования:')
        Ent2 = Entry(root2, width = 30)
        Ent3 = Entry(root2, width = 30)
        Btn2 = Button(root2, text = 'Добавить', command = add2)
        Lbl2.grid(row = 0, column = 5)
        Lbl3.grid(row = 1, column = 5)
        Ent2.grid(row = 0, column = 6)
        Ent3.grid(row = 1, column = 6)
        Btn2.grid(row = 2, column = 5)
def updateLists():
    def add():
        numb = Ent1.get()
        value = Ent2.get()
        try:
            cur = con.cursor()
            cur.execute(f"call update_listofequipment({numb}, '{value}')")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()


    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Обновление списков оборудования')
    Lbl1 = Label(root2, text = 'Введите номер:')
    Lbl2 = Label(root2, text = 'Введите название:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 1, column = 0)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 1, column = 1)
    Btn1.grid(row = 2, column = 0)   
def deleteLists():
    def delete1():
        def delete():
            numb = Ent1.get()
            try:
                cur = con.cursor()
                cur.execute(f"call delete_from_listofequipment({numb})")
                cur.close()
                con.commit()
            except:
                con.rollback()
                mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения.\n Проверьте, есть ли связи удаляемой записи с другими таблицами.")
            root3.destroy()


        root3 = Tk()
        root3.geometry('480x270')
        root3.title('Удаление списка оборудования')
        Lbl1 = Label(root3, text = 'Введите номер:')
        Ent1 = Entry(root3, width = 30)
        Btn1 = Button(root3, text = 'Удалить', command = delete)
        Lbl1.grid(row = 0, column = 0)
        Ent1.grid(row = 0, column = 1)
        Btn1.grid(row = 0, column = 2)

    def delete2():
        def delete():
            numb = Ent1.get()
            try:
                cur = con.cursor()
                cur.execute(f"call delete_from_inlist({numb})")
                cur.close()
                con.commit()
            except:
                con.rollback()
                mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения.\n Проверьте, есть ли связи удаляемой записи с другими таблицами.")
            root3.destroy()


        root3 = Tk()
        root3.geometry('480x270')
        root3.title('Удаление оборудования из спика')
        Lbl1 = Label(root3, text = 'Введите номер:')
        Ent1 = Entry(root3, width = 30)
        Btn1 = Button(root3, text = 'Удалить', command = delete)
        Lbl1.grid(row = 0, column = 0)
        Ent1.grid(row = 0, column = 1)
        Btn1.grid(row = 0, column = 2)



    root2 = Tk()
    root2.geometry('480x270')
    root2.title('Удаление списка обородования|из списка оборудования')
    Btn1 = Button(root2, text = 'Удалить список', command = delete1)
    Btn2 = Button(root2, text = 'Удалить оборудование', command = delete2)
    Btn1.grid(row = 0, column = 0)
    Btn2.grid(row = 0, column = 1)

def showEquipment():
    clearEquipment()
    Lbl1 = Label(TabEquipment, text = 'Номер')
    Lbl2 = Label(TabEquipment, text = 'Название')
    Lbl3 = Label(TabEquipment, text = 'Локация')
    Lbl4 = Label(TabEquipment, text = 'Купивший')
    Lbl5 = Label(TabEquipment, text = 'Ур.доступа')
    Lbl6 = Label(TabEquipment, text = 'ответственный')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    Lbl5.grid(row = 1, column = 4)
    Lbl6.grid(row = 1, column = 5)
    cur = con.cursor()
    cur.execute("select * from equipment order by number")
    rows = cur.fetchall()
    cur.close()
    List = list()
    Label_list = list()
    for i in range(len(rows)):
        List.append(rows[i])
    for i in range(len(List)):
        for j in range(len(List[i])):
            Label_list.append(Label(TabEquipment, text = List[i][j]))
    j = 2
    for i in range(0, len(List) * 6, 6):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        Label_list[i + 4].grid(row = j, column = 4)
        Label_list[i + 5].grid(row = j, column = 5)
        j += 1
def clearEquipment():
    list = TabEquipment.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 4:
            l.destroy()
def addEquipment():
    def add():
        name = Ent1.get()
        locate = Ent2.get()
        buyer = Ent3.get()
        allow_level = Ent4.get()
        responsible = Ent5.get()
        try:
            cur = con.cursor()
            cur.execute(f"call add_to_equipment('{name}',{locate},{buyer},{allow_level},{responsible})")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()



    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление оборудования')
    Lbl1 = Label(root2, text = 'Введите название:')
    Lbl2 = Label(root2, text = 'Введите расположение:')
    Lbl3 = Label(root2, text = 'Введите закупившего:')
    Lbl4 = Label(root2, text = 'Введите ур.доступа:')
    Lbl5 = Label(root2, text = 'Введите ответственного:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Ent3 = Entry(root2, width = 30)
    Ent4 = Entry(root2, width = 30)
    Ent5 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 1, column = 0)
    Lbl3.grid(row = 2, column = 0)
    Lbl4.grid(row = 3, column = 0)
    Lbl5.grid(row = 4, column = 0)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 1, column = 1)
    Ent3.grid(row = 2, column = 1)
    Ent4.grid(row = 3, column = 1)
    Ent5.grid(row = 4, column = 1)
    Btn1.grid(row = 5, column = 0)
def updateEquipment():
    def add():
        numb = Ent1.get()
        name = Ent2.get()
        locate = Ent3.get()
        buyer = Ent4.get()
        allow_level = Ent5.get()
        responsible = Ent6.get()
        try:
            cur = con.cursor()
            if len(name) > 0:
                cur.execute(f"call update_equipment1({numb}, '{name}')")
            if len(locate) > 0:
                cur.execute(f"call update_equipment2({numb}, {locate})")
            if len(buyer) > 0:
                cur.execute(f"call update_equipment3({numb}, {buyer})")
            if len(allow_level) > 0:
                cur.execute(f"call update_equipment4({numb}, {allow_level})")
            if len(responsible) > 0:
                cur.execute(f"call update_equipment5({numb}, {responsible})")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()



    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление оборудования')
    Lbl1 = Label(root2, text = 'Введине номер:')
    Lbl2 = Label(root2, text = 'Введите название:')
    Lbl3 = Label(root2, text = 'Введите расположение:')
    Lbl4 = Label(root2, text = 'Введите закупившего:')
    Lbl5 = Label(root2, text = 'Введите ур.доступа:')
    Lbl6 = Label(root2, text = 'Введите ответственного:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Ent3 = Entry(root2, width = 30)
    Ent4 = Entry(root2, width = 30)
    Ent5 = Entry(root2, width = 30)
    Ent6 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 0, column = 2)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 2, column = 2)
    Lbl5.grid(row = 3, column = 2)
    Lbl6.grid(row = 4, column = 2)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 0, column = 3)
    Ent3.grid(row = 1, column = 3)
    Ent4.grid(row = 2, column = 3)
    Ent5.grid(row = 3, column = 3)
    Ent6.grid(row = 4, column = 3)
    Btn1.grid(row = 5, column = 2) 
def deleteEquipment():
    def delete():
        numb = Ent1.get()
        try:
            cur = con.cursor()
            if numb > 1:
                cur.execute(f"select correct_equipment_delete({numb})")
            cur.execute(f"select correct_delete_equipment({numb})")
            cur.execute(f"call delete_from_equipment({numb})")
            cur.close()
            con.commit()
        except:
            con.rollback()
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения.\n Проверьте, есть ли связи удаляемой записи с другими таблицами.")
        root2.destroy()


    root2 = Tk()
    root2.geometry('480x270')
    root2.title('Удаление оборудования')
    Lbl1 = Label(root2, text = 'Введите номер:')
    Ent1 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Удалить', command = delete)
    Lbl1.grid(row = 0, column = 0)
    Ent1.grid(row = 0, column = 1)
    Btn1.grid(row = 0, column = 2)

def deleteAllows():
    def delete():
        numb = Ent1.get()
        try:
            cur = con.cursor()
            cur.execute(f"select correct_allows_members({numb})")
            cur.execute(f"select correct_allows_locations({numb})")
            cur.execute(f"select correct_allows_equipment({numb})")
            cur.execute(f"select correct_allows_researches({numb})")
            cur.execute(f"call delete_from_allows({numb})")
            cur.close()
            con.commit()
        except:
            con.rollback()
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения.\n Проверьте, есть ли связи удаляемой записи с другими таблицами.")
        root2.destroy()


    root2 = Tk()
    root2.geometry('480x270')
    root2.title('Удаление уровня доступа')
    Lbl1 = Label(root2, text = 'Введите номер:')
    Ent1 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Удалить', command = delete)
    Lbl1.grid(row = 0, column = 0)
    Ent1.grid(row = 0, column = 1)
    Btn1.grid(row = 0, column = 2)
def updateAllows():
    def add():
        numb = Ent1.get()
        value = Ent2.get()
        try:
            cur = con.cursor()
            cur.execute(f"select update_allows({numb}, '{value}')")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()


    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Обновление уровней допуска')
    Lbl1 = Label(root2, text = 'Введите номер:')
    Lbl2 = Label(root2, text = 'Введите значение:')
    Ent1 = Entry(root2, width = 30)
    Ent2 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Lbl2.grid(row = 1, column = 0)
    Ent1.grid(row = 0, column = 1)
    Ent2.grid(row = 1, column = 1)
    Btn1.grid(row = 2, column = 0)    
def addAllows():
    def add():
        numb = Ent1.get()
        try:
            cur = con.cursor()
            cur.execute(f"call add_to_allows('{numb}')")
            cur.close()
            con.commit()
        except:
            mb.showerror("Ошибка", "Проверьте правильность данных, формата, подключения и соединения")
        root2.destroy()


    root2 = Tk()
    root2.geometry('960x540')
    root2.title('Добавление уровней допуска')
    Lbl1 = Label(root2, text = 'Введите значение:')
    Ent1 = Entry(root2, width = 30)
    Btn1 = Button(root2, text = 'Добавить', command = add)
    Lbl1.grid(row = 0, column = 0)
    Ent1.grid(row = 0, column = 1)
    Btn1.grid(row = 0, column = 2)  
def showAllows():
    clearAllows()
    Lbl1 = Label(TabAllows, text = 'Приоритет')
    Lbl2 = Label(TabAllows, text = 'Название')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    cur = con.cursor()
    cur.execute("select * from allows order by number")
    rows = cur.fetchall()
    cur.close()
    List = list()
    Label_list = list()
    for i in range(len(rows)):
        List.append(rows[i])
    for i in range(len(List)):
        for j in range(len(List[i])):
            Label_list.append(Label(TabAllows, text = List[i][j]))
    j = 2
    for i in range(0, len(List) * 2, 2):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        j += 1
def clearAllows():
    list = TabAllows.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 4:
            l.destroy()

def clicked2E():
    clear2E()
    numb = Tht51.get()
    Lbl1 = Label(Tab2E, text = 'Название')
    Lbl2 = Label(Tab2E, text = 'Локация')
    Lbl3 = Label(Tab2E, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    cur = con.cursor()
    cur.execute(f"select two_e({numb})")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2E, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 3, 3):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        j += 1
def clear2E():
    list = Tab2E.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 3:
            l.destroy()

def clicked2D():
    clear2D()
    Lbl1 = Label(Tab2D, text = 'Фамилия')
    Lbl2 = Label(Tab2D, text = 'Имя')
    Lbl3 = Label(Tab2D, text = 'Отчество')
    Lbl4 = Label(Tab2D, text = 'Количество проектов')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_d()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2D, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clear2D():
    list = Tab2D.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 1:
            l.destroy()

def clicked2C6():
    clear2C()
    Lbl1 = Label(Tab2C, text = 'Фамилия')
    Lbl2 = Label(Tab2C, text = 'Имя')
    Lbl3 = Label(Tab2C, text = 'Отчество')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    cur = con.cursor()
    cur.execute("select two_c6()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2C, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 3, 3):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        j += 1
def clicked2C5():
    clear2C()
    Lbl1 = Label(Tab2C, text = 'Фамилия')
    Lbl2 = Label(Tab2C, text = 'Имя')
    Lbl3 = Label(Tab2C, text = 'Отчество')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    cur = con.cursor()
    cur.execute("select two_c5()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2C, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 3, 3):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        j += 1
def clicked2C4():
    clear2C()
    Lbl1 = Label(Tab2C, text = 'Название')
    Lbl2 = Label(Tab2C, text = 'Локация')
    Lbl3 = Label(Tab2C, text = 'Ур.доступа')
    Lbl4 = Label(Tab2C, text = 'Список')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_c4()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2C, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2C3():
    clear2C()
    Lbl1 = Label(Tab2C, text = 'Название')
    Lbl2 = Label(Tab2C, text = 'Локация')
    Lbl3 = Label(Tab2C, text = 'Ур.доступа')
    Lbl4 = Label(Tab2C, text = 'Список')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_c3()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2C, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2C2():
    clear2C()
    Lbl1 = Label(Tab2C, text = 'Список')
    Lbl2 = Label(Tab2C, text = 'Оборудование')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    cur = con.cursor()
    cur.execute("select two_c2()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2C, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 2, 2):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        j += 1
def clicked2C1():
    clear2C()
    Lbl1 = Label(Tab2C, text = 'Номер')
    Lbl2 = Label(Tab2C, text = 'Список')
    Lbl3 = Label(Tab2C, text = 'Оборудование')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    cur = con.cursor()
    cur.execute("select two_c1()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2C, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 3, 3):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        j += 1
def clear2C():
    list = Tab2C.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 6:
            l.destroy()

def clicked2B1():
    clear2B()
    Lbl1 = Label(Tab2B, text = 'Фамилия')
    Lbl2 = Label(Tab2B, text = 'Имя')
    Lbl3 = Label(Tab2B, text = 'Отчество')
    Lbl4 = Label(Tab2B, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_b1()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2B, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2B2():
    clear2B()
    Lbl1 = Label(Tab2B, text = 'Фамилия')
    Lbl2 = Label(Tab2B, text = 'Имя')
    Lbl3 = Label(Tab2B, text = 'Отчество')
    Lbl4 = Label(Tab2B, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_b2()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2B, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2B3():
    clear2B()
    Lbl1 = Label(Tab2B, text = 'Фамилия')
    Lbl2 = Label(Tab2B, text = 'Имя')
    Lbl3 = Label(Tab2B, text = 'Отчество')
    Lbl4 = Label(Tab2B, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_b3()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2B, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2B4():
    clear2B()
    Lbl1 = Label(Tab2B, text = 'Фамилия')
    Lbl2 = Label(Tab2B, text = 'Имя')
    Lbl3 = Label(Tab2B, text = 'Отчество')
    Lbl4 = Label(Tab2B, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_b4()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2B, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2B5():
    clear2B()
    Lbl1 = Label(Tab2B, text = 'Фамилия')
    Lbl2 = Label(Tab2B, text = 'Имя')
    Lbl3 = Label(Tab2B, text = 'Отчество')
    Lbl4 = Label(Tab2B, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_b5()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2B, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2B6():
    clear2B()
    Lbl1 = Label(Tab2B, text = 'Фамилия')
    Lbl2 = Label(Tab2B, text = 'Имя')
    Lbl3 = Label(Tab2B, text = 'Отчество')
    Lbl4 = Label(Tab2B, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_b6()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2B, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2B7():
    clear2B()
    Lbl1 = Label(Tab2B, text = 'Фамилия')
    Lbl2 = Label(Tab2B, text = 'Имя')
    Lbl3 = Label(Tab2B, text = 'Отчество')
    Lbl4 = Label(Tab2B, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_b7()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2B, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clicked2B8():
    clear2B()
    Lbl1 = Label(Tab2B, text = 'Фамилия')
    Lbl2 = Label(Tab2B, text = 'Имя')
    Lbl3 = Label(Tab2B, text = 'Отчество')
    Lbl4 = Label(Tab2B, text = 'Ур.допуска')
    Lbl1.grid(row = 1, column = 0)
    Lbl2.grid(row = 1, column = 1)
    Lbl3.grid(row = 1, column = 2)
    Lbl4.grid(row = 1, column = 3)
    cur = con.cursor()
    cur.execute("select two_b8()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2B, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2) * 4, 4):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        j += 1
def clear2B():
    list = Tab2B.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 8:
            l.destroy()

def clicked2A():
    clicked_clear2A()
    Lbl11 = Label(Tab2A, text = 'Название')
    Lbl12 = Label(Tab2A, text = 'Локация')
    Lbl13 = Label(Tab2A, text = 'Фамилия')
    Lbl14 = Label(Tab2A, text = 'Имя')
    Lbl15 = Label(Tab2A, text = 'Отчество')
    Lbl16 = Label(Tab2A, text = 'Ур.доступа')
    Lbl11.grid(row = 1, column = 0)
    Lbl12.grid(row = 1, column = 1)
    Lbl13.grid(row = 1, column = 2)
    Lbl14.grid(row = 1, column = 3)
    Lbl15.grid(row = 1, column = 4)
    Lbl16.grid(row = 1, column = 5)
    cur = con.cursor()
    cur.execute("select two_a()")
    rows = cur.fetchall()
    cur.close()
    save = list()
    List = list()
    List2 = list()
    Label_list = list()
    for i in range(len(rows)):
        save.append(list(rows[i]))
    for i in range(len(save)):
        List.append(list(save[i][0]))
    for i in range(len(List)):
        List[i].pop(0)
        List[i].pop(-1)
        List[i] = ''.join(List[i])
        print(List[i])
    for i in range(len(List)):
        List2.append(List[i].split(','))
    for i in range(len(List2)):
        for j in range(len(List2[i])):
            Label_list.append(Label(Tab2A, text = List2[i][j]))
    j = 2
    for i in range(0, len(List2)*6, 6):
        Label_list[i].grid(row = j, column = 0)
        Label_list[i + 1].grid(row = j, column = 1)
        Label_list[i + 2].grid(row = j, column = 2)
        Label_list[i + 3].grid(row = j, column = 3)
        Label_list[i + 4].grid(row = j, column = 4)
        Label_list[i + 5].grid(row = j, column = 5)
        j += 1
def clicked_clear2A():
    list = Tab2A.grid_slaves()
    i = 0
    for l in list:
        i+=1
        if i < len(list) - 1:
            l.destroy()




n = 0
while(n == 0):
    print('Введите логин:')
    login = input()
    print('Введите пароль:')
    password2 = input()
    try:
        con = psycopg2.connect(
                host = '127.0.0.1',
                database = 'metrological_service',
                user = login,
                password = password2,
                port = 5432
            )
        n = 1
    except:
        print('Проверьте данные')



con = psycopg2.connect(
            host = '127.0.0.1',
            database = 'metrological_service',
            user = login,
            password = password2,
            port = 5432
        )

root = Tk()
root.geometry('1920x1080')
root.title('Программа')
tab_control = ttk.Notebook(root)  
Tab2A = ttk.Frame(tab_control)  
tab_control.add(Tab2A, text='2A')
Tab2B = ttk.Frame(tab_control)  
tab_control.add(Tab2B, text='2B')
Tab2C = ttk.Frame(tab_control)  
tab_control.add(Tab2C, text='2C')
Tab2D = ttk.Frame(tab_control)  
tab_control.add(Tab2D, text='2D')
Tab2E = ttk.Frame(tab_control)  
tab_control.add(Tab2E, text='2E')
TabAllows = ttk.Frame(tab_control)   
tab_control.add(TabAllows, text='Allows')
TabEquipment = ttk.Frame(tab_control)  
tab_control.add(TabEquipment, text='Equipment')
TabLists = ttk.Frame(tab_control)  
tab_control.add(TabLists, text='Lists')
TabLocations = ttk.Frame(tab_control)  
tab_control.add(TabLocations, text='Locations')
TabMembers = ttk.Frame(tab_control)  
tab_control.add(TabMembers, text='Members')
TabResearches = ttk.Frame(tab_control)  
tab_control.add(TabResearches, text='Researches')
tab_control.pack(expand=1, fill='both')  



Btn11 = Button(Tab2A, text = 'Отобразить', command = clicked2A)
Btn12 = Button(Tab2A, text = 'Очистить', command = clicked_clear2A)
Btn11.grid(row = 0, column = 0)
Btn12.grid(row = 0, column = 1)



Btn21 = Button(Tab2B, text = 'View1', command = clicked2B1)
Btn22 = Button(Tab2B, text = 'View2', command = clicked2B2)
Btn23 = Button(Tab2B, text = 'View3', command = clicked2B3)
Btn24 = Button(Tab2B, text = 'View4', command = clicked2B4)
Btn25 = Button(Tab2B, text = 'View5', command = clicked2B5)
Btn26 = Button(Tab2B, text = 'View6', command = clicked2B6)
Btn27 = Button(Tab2B, text = 'View7', command = clicked2B7)
Btn28 = Button(Tab2B, text = 'View8', command = clicked2B8)
Btn29 = Button(Tab2B, text = 'Очистить', command = clear2B)
Btn21.grid(row = 0, column = 0)
Btn22.grid(row = 0, column = 1)
Btn23.grid(row = 0, column = 2)
Btn24.grid(row = 0, column = 3)
Btn25.grid(row = 0, column = 4)
Btn26.grid(row = 0, column = 5)
Btn27.grid(row = 0, column = 6)
Btn28.grid(row = 0, column = 7)
Btn29.grid(row = 0, column = 8)


Btn31 = Button(Tab2C, text = '2C1', command = clicked2C1)
Btn32 = Button(Tab2C, text = '2C2', command = clicked2C2)
Btn33 = Button(Tab2C, text = '2C3', command = clicked2C3)
Btn34 = Button(Tab2C, text = '2C4', command = clicked2C4)
Btn35 = Button(Tab2C, text = '2C5', command = clicked2C5)
Btn36 = Button(Tab2C, text = '2C6', command = clicked2C6)
Btn37 = Button(Tab2C, text = 'Очистить', command = clear2C)
Btn31.grid(row = 0, column = 0)
Btn32.grid(row = 0, column = 1)
Btn33.grid(row = 0, column = 2)
Btn34.grid(row = 0, column = 3)
Btn35.grid(row = 0, column = 4)
Btn36.grid(row = 0, column = 5)
Btn37.grid(row = 0, column = 6)


Btn41 = Button(Tab2D, text = 'Отобразить', command = clicked2D)
Btn42 = Button(Tab2D, text = 'Очистить', command = clear2D)
Btn41.grid(row = 0, column = 0)
Btn42.grid(row = 0, column = 1)


Lbl51 = Label(Tab2E, text = 'Выберите сотрудника')
Lbl51.grid(row = 0, column = 0)
Tht51 = Entry(Tab2E, width = 30)
Tht51.grid(row = 0, column = 1)
Btn51 = Button(Tab2E, text = 'Отобразить', command = clicked2E)
Btn52 = Button(Tab2E, text = 'Очистить', command= clear2E)
Btn51.grid(row = 0, column = 2)
Btn52.grid(row = 0, column = 3)


Btn61 = Button(TabAllows, text = 'Отобразить', command = showAllows)
Btn62 = Button(TabAllows, text = 'Очистить', command = clearAllows)
Btn63 = Button(TabAllows, text = 'Добавить', command = addAllows)
Btn64 = Button(TabAllows, text = 'Изменить', command = updateAllows)
Btn65 = Button(TabAllows, text = 'Удалить', command = deleteAllows)
Btn61.grid(row = 0, column = 0)
Btn62.grid(row = 0, column = 1)
Btn63.grid(row = 0, column = 2)
Btn64.grid(row = 0, column = 3)
Btn65.grid(row = 0, column = 4)


Btn71 = Button(TabEquipment, text = 'Отобразить', command = showEquipment)
Btn72 = Button(TabEquipment, text = 'Очистить', command = clearEquipment)
Btn73 = Button(TabEquipment, text = 'Добавить', command = addEquipment)
Btn74 = Button(TabEquipment, text = 'Изменить', command = updateEquipment)
Btn75 = Button(TabEquipment, text = 'Удалить', command = deleteEquipment)
Btn71.grid(row = 0, column = 0)
Btn72.grid(row = 0, column = 1)
Btn73.grid(row = 0, column = 2)
Btn74.grid(row = 0, column = 3)
Btn75.grid(row = 0, column = 4)


Btn81 = Button(TabLists, text = 'Отобразить', command = showLists)
Btn82 = Button(TabLists, text = 'Очистить', command = clearLists)
Btn83 = Button(TabLists, text = 'Добавить', command = addLists)
Btn84 = Button(TabLists, text = 'Изменить', command = updateLists)
Btn85 = Button(TabLists, text = 'Удалить', command = deleteLists)
Btn81.grid(row = 0, column = 0)
Btn82.grid(row = 0, column = 1)
Btn83.grid(row = 0, column = 2)
Btn84.grid(row = 0, column = 3)
Btn85.grid(row = 0, column = 4)


Btn91 = Button(TabLocations, text = 'Отобразить', command = showLocations)
Btn92 = Button(TabLocations, text = 'Очистить', command = clearLocations)
Btn93 = Button(TabLocations, text = 'Добавить', command = addLocations)
Btn94 = Button(TabLocations, text = 'Изменить', command = updateLocations)
Btn95 = Button(TabLocations, text = 'Удалить', command = deleteLocations)
Btn91.grid(row = 0, column = 0)
Btn92.grid(row = 0, column = 1)
Btn93.grid(row = 0, column = 2)
Btn94.grid(row = 0, column = 3)
Btn95.grid(row = 0, column = 4)


Btn101 = Button(TabMembers, text = 'Отобразить', command = showMembers)
Btn102 = Button(TabMembers, text = 'Очистить', command = clearMembers)
Btn103 = Button(TabMembers, text = 'Добавить', command = addMembers)
Btn104 = Button(TabMembers, text = 'Изменить', command = updateMembers)
Btn105 = Button(TabMembers, text = 'Удалить', command = deleteMembers)
Btn101.grid(row = 0, column = 0)
Btn102.grid(row = 0, column = 1)
Btn103.grid(row = 0, column = 2)
Btn104.grid(row = 0, column = 3)
Btn105.grid(row = 0, column = 4)


Btn111 = Button(TabResearches, text = 'Отобразить', command = showResearches)
Btn112 = Button(TabResearches, text = 'Очистить', command = clearResearches)
Btn113 = Button(TabResearches, text = 'Добавить', command = addResearches)
Btn114 = Button(TabResearches, text = 'Изменить', command = updateResearches)
Btn115 = Button(TabResearches, text = 'Удалить', command = deleteResearches)
Btn116 = Button(TabResearches, text = 'Изменить курсором', command = updatewithcursor)
Btn111.grid(row = 0, column = 0)
Btn112.grid(row = 0, column = 1)
Btn113.grid(row = 0, column = 2)
Btn114.grid(row = 0, column = 3)
Btn115.grid(row = 0, column = 4)
Btn116.grid(row = 0, column = 5)





root.mainloop()
